let life = 280; // cantidad de ticks maximos de vida que tienen las hormigas
// clase principal del algoritmo genetico
function Population(s) {
    this.generation = 0;
    this.population = [];
    this.size = s;
    this.breedSize = 30;
    this.mutationRate = 0.005;
    for (let i = 0; i < this.size; i++) {
        this.population[i] = new Ant();
    }
    // funcion simple de crossover, toma al azar un elemento del padre o madre y se lo da al hijo
    this.crossover = function() {
        if (this.eval()) {
            let best = this.getBest();
            let offSpring = [];
            for (let i = 0; i < this.size; i++) {
                parentA = random(best);
                parentB = random(best);
                child = new Ant();
                for (let j = 0; j < parentA.dna.size; j++) {
                    if (random(1) < this.mutationRate) {
                        child.dna.info[j] = p5.Vector.random2D();
                        child.dna.info[j].setMag(random(0.35))
                    } else {
                        child.dna.info[j] = random([parentA.dna.info[j], parentB.dna.info[j]])
                    }                    
                }
                offSpring[i] = child;
            }
            this.population = offSpring;
            this.generation++;
            timer.time = life;
        }
    }
    // entrega una lista con los mejores N elementos de una generacion
    this.getBest = function() {
        this.setRank();
        let best = [];
        let index = 0;
        for (let i = 0; i < this.size; i++) {
            if (this.population[i].rank > this.size - this.breedSize && index < this.breedSize) {
                best[index] = this.population[i];
                index++;
            }
        }
        while (best.length < this.breedSize) {
            best.push(random(this.population));
        }
        return best;
    }
    // settea el rank de cada elemento de la poblacion, mas es mejor
    this.setRank = function() {
        for (let i = 0; i < this.size; i++) {
            this.population[i].setFitness(food.position);
        }
        for (let i = 0; i < this.size; i++) {
            for (let j = 0; j < this.size; j++) {
                if (this.population[i].fitness > this.population[j].fitness) {
                    this.population[i].rank += 1;
                }
            }
        }
        if (setRankFlag) {
            let maxRank = 0;
            for (let i = 0; i < this.size; i++) {
                if (this.population[i].rank > maxRank) {
                    maxRank = this.population[i].rank;
                    maxIndex = i;
                }
            }
        }        
    }
    // checkea si la simulacion esta lista para hacer crossover
    this.eval = function() {
        for (let i = 0; i < this.size; i++) {
            if (!this.population[i].finished) {
                return false;
            }
        }
        return true;
    }
    // settea el modo showcase para mostrar el mejor elemento
    this.simBest = function() {
        if (this.eval() && setRankFlag == true){
            this.setRank();
            this.population[maxIndex].reset();
            setRankFlag = false;
            gotMax = true;
            breeding = false;
        }
    }
}
// clase de los individuos
function Ant() {    
    this.dna = new DNA(life);
    // propiedades fisicas de la hormiga
    this.position = createVector(startingPos[0], startingPos[1]);
    this.vel = createVector(0,0);
    this.acc = createVector(0,0);

    this.finished = false;
    this.fitness = -1;
    this.rank = 1;
    this.tick = 0;
    this.path = [];
    // se encarga del dibujo una hormiga
    this.draw = function() {
        if (breeding == false) {
            this.drawPath();
        }
        if (!this.finished) {
            let animSpeed = floor(this.tick / 10) % 3;
            push();
            translate(this.position.x, this.position.y);
            rotate(this.vel.heading() + PI / 2);
            imageMode(CENTER);
            scale(1.2)
            image(animation[animSpeed], 0, 0);
            pop();
        }
    }
    // se encarga de las 'fisicas' y movimiento de la hormiga
    this.update = function() {
        this.isFinished();
        if (this.finished == false) {
            this.acc.add(this.dna.info[this.tick]);
            this.vel.add(this.acc);
            this.position.add(this.vel);
            this.acc.mult(0);
            this.tick++;
            if (this.tick % 20 == 0) {
                this.path.push([this.position.x, this.position.y]);
            }
        } else if (this.fitness == -1) {
            this.setFitness(food.position);
        }
    }
    // funcion que determina si una hormiga termino su movimiento
    this.isFinished = function() {
        if (distance(this.position, food.position) < 36) {
            console.log("asd")
            this.finished = true;
            return;
        }
        if (this.position.x < 0 || this.position.x > width || this.position.y < 0 || this.position.y > height) {
            this.finished = true;
            return;
        }
        if (this.tick > life) {
            this.finished = true;
            return;
        }
        for (let i = 0; i < walls.length; i++) {
            if (walls[i].inArea(this.position)){
                this.finished = true;
            }
        }
    }
    // funcion simple que calcula el fitness como la inversa de la distancia
    this.setFitness = function(target) {
        let dist = distance(this.position, target) + 1;
        this.fitness = 1 / dist;
    }
    // en el modo showcase: dibuja un spline de catmull-rom con puntos de la trayectoria
    this.drawPath = function() {
        noFill();
        stroke(0,255,0);
        strokeWeight(2);
        if (this.path.length > 2) {
            beginShape();
            curveVertex(this.path[0][0], this.path[0][1]);
            for (let i = 0; i < this.path.length; i++) {
                curveVertex(this.path[i][0], this.path[i][1]);
            }
            curveVertex(this.path[this.path.length-1][0], this.path[this.path.length-1][1]);
            endShape();
        }
    }
    // reinicia el estado de una hormiga al inicial
    this.reset = function() {
        this.position.x = startingPos[0];
        this.position.y = startingPos[1];
        this.finished = false;
        this.tick = 0;
        this.path = [];
        this.vel.mult(0);
        this.acc.mult(0);
    }
}
// clase que contiene el codigo genetico de una hormiga (los vectores fuerza que la hacen mover)
function DNA(size) {
    this.size = size;
    this.info = [];
    for (let i = 0; i < this.size; i++) {
        this.info[i] = p5.Vector.random2D();
        this.info[i].setMag(random(0.35))
    }
}
// clase simple que contiene la posicion y cualidades graficas del objetivo (comida)
function Food() {
    this.position = createVector(goalPos[0], goalPos[1])
    this.draw = function() {
        let yOffset = 4 * sin(frameCount / 30);
        let xOffset = 4 * cos(-frameCount / 30);
        stroke(0);
        strokeWeight(1);
        fill(10,200,50);
        // ellipse(this.position.x, this.position.y, 30);
        
        push();
        translate(this.position.x, this.position.y);
        imageMode(CENTER)
        let animSpeed = floor(frameCount / 3);
        scale(0.7);
        image(foodAnimation[animSpeed%15], -xOffset, yOffset);
        pop();
    }
}
// distancia euclidiana para calcular fitness
function distance(v1, v2) {
    return sqrt(sq(v1.x - v2.x) + sq(v1.y - v2.y));
}
// clase simple para dibujar paredes
function Wall(px, py, w, h) {
    this.x = px;
    this.y = py;
    this.width = w;
    this.height = h;

    this.draw = function() {
        stroke(0);
        strokeWeight(2);
        rect(this.x, this.y, this.width, this.height);
        imageMode(CORNER);
        image(wall, this.x, this.y, this.width, this.height/3);
        image(wall, this.x, this.y + this.height/3, this.width, this.height/3);
        image(wall, this.x, this.y + 2*this.height/3, this.width, this.height/3);
    }
    // determina si un punto esta dentro de esta pared para realizar colisiones
    this.inArea = function(point) {
        if (point.x < this.x) {
            return false;
        } else if (point.x > this.x + this.width) {
            return false;
        } else if (point.y < this.y) {
            return false;
        } else if (point.y > this.y + this.height) {
            return false;
        } else {
            return true;
        }
    }
}
// clase ilustrativa para mostrar cuanto tiempo de vida le queda a una generacion
function Timer() {
    this.time = life;
    this.update = function() {
        this.time -= 1;
        if (this.time <= 0) {
            this.time = life;
        }
    }
}