let speedSlider; // slider de velocidad
let mutationSlider; // silder de mutacion %
breeding = true; // variable flag que determina si estoy en showcase o no
// variables 'flag' para el modo showcase
let maxIndex;
let setRankFlag = false;
let gotMax = false;
// variables necesarias para la animacion de las hormigas y comida
let spriteData;
let spriteSheet;
let animation;
let foodAnimation;
// posiciones iniciales y finales
let startingPos;
let goalPos;
let walls = [];

function preload() {
    grass = loadImage("assets/grass.png");
    wall = loadImage("assets/wall1.jpg")
    spriteData = loadJSON("assets/ant.json");
    spriteSheet = loadImage("assets/antT.png");
    foodSheet = loadImage("assets/food.png");
    objectSettings = loadStrings('settings.txt');
}

function setup() {
    setSettings(); // load settings
    createCanvas(800, 800); // ventana de 800x800
    smooth(); // antialliasing
    p = new Population(200);
    food = new Food();
    timer = new Timer();

    // crear los sliders
    mutationSlider = createSlider(0.0, 0.02, 0.005, 0.001);
    mutationSlider.position(300, height);
    mutationSlider.style("background-color", "#008744");
    speedSlider = createSlider(1, 20, 2, 1);
    speedSlider.position(0, height);
    speedSlider.style("background-color", "#4a6ca8");

    // genera arreglo de frames para la animacion de la hormiga
    animation = [];
    for (let i = 0; i < spriteData.frames.length; i++) {
        let pos = spriteData.frames[i].position;
        let img = spriteSheet.get(pos.x, pos.y, pos.w, pos.h);
        animation[i] = img;
    }
    // genera arreglo de frames para la animacion del tomate
    foodAnimation = [];
    for (let i = 0; i < 5; i++) {
        for (let j = 0; j < 3; j++) {
            let posx = i * 85;
            let posy = j * 0;
            let img = foodSheet.get(posx, posy, 85, 85);
            foodAnimation.push(img);
        }
    }
}
// loop de logica y dibujo
function draw() {
    let speed = speedSlider.value(); // velocidad de simulacion
    p.mutationRate = mutationSlider.value(); // actualizar la probabilidad de mutacion
    image(grass, 0, 0, width, height); // background
    
    if (breeding) { // si se esta en modo de evolucion
        p.population.forEach(elem => {
            for (let i = 0; i < speed; i++) {
                elem.update();
            }
            elem.draw();
        });
        p.simBest();
        if (breeding) {
            p.crossover();
        }        
    } else { // si se esta en modo showcase (mostrando el mejor)
        if (gotMax) {
            if (p.population[maxIndex].finished) {
                p.population[maxIndex].reset();
            }
            p.population[maxIndex].update();
            p.population[maxIndex].draw();
        }        
    }
    walls.forEach(elem => {
        elem.draw(); // dibujar cada pared
    });
    food.draw();
    for (let i = 0; i < speed; i++) {
        timer.update();
    }
    // texto de informacion
    stroke(0);
    strokeWeight(2);
    fill(240);
    textSize(32);
    text("generation : " + p.generation, 450, 50);
    if (breeding) {
        text("mutationRate : " + p.mutationRate, 450, 80)
        text("speed : " + speed, 450, 110);
        text("life: " + timer.time, 450, 140)
    }
}
// define las posiciones segun lo indicado en settings.txt
function setSettings() {
    startingPos = objectSettings[0].split(" ");
    goalPos = objectSettings[1].split(" ");
    for (let i = 0; i < 2; i++) {
        startingPos[i] = parseInt(startingPos[i]);
        goalPos[i] = parseInt(goalPos[i]);
    }
    for (let i = 2; i < objectSettings.length; i++) {
        let coords = objectSettings[i].split(" ");
        let x1 = parseInt(coords[0]);
        let y1 = parseInt(coords[1]);
        let w = parseInt(coords[2]);
        let h = parseInt(coords[3]);
        walls.push(new Wall(x1, y1, w, h));
    }
}
function keyPressed() {
    // cambia de modo evolucion a showcase
    if (keyCode == 32) { // SPACE
        if (breeding) {
            setRankFlag = true;
            gotMax = false;
        } else {
            breeding = true;
            gotMax = false;
        }
    }
    // cambia valores de los sliders:
    // velocidad de simulacion
    if (keyCode == LEFT_ARROW) {
        let val = speedSlider.value();
        speedSlider.value(val - 1);
    } else if (keyCode == RIGHT_ARROW) {
        let val = speedSlider.value();
        speedSlider.value(val + 1);
    // probabilidad de mutacion
    } else if (keyCode == DOWN_ARROW) {
        let val = mutationSlider.value();
        mutationSlider.value(val - 0.001);
    } else if (keyCode == UP_ARROW) {
        let val = mutationSlider.value();
        mutationSlider.value(val + 0.001);
    }
}